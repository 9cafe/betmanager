let bankData = [];
fetch(url)
	.then(r => r.json())
	.then(bets => {
		const stake = 100;
		let bank = 1000;
		bankData = bets.reverse().reduce((data, bet) => {
			const [date, odds, results] = bet;
			const isLost = results.some(v => v === 0);
			const hasMoney = bank > stake;
			if (!hasMoney) return data;
			const totalOdd = odds.reduce((oddAcc, odd, idx) => {
				if (results[idx] === 2) return oddAcc;
				oddAcc *= odd / 100;
				return oddAcc;
			}, 1);
			const bankBefore = bank;
			bank = !isLost ? bank + stake * totalOdd - stake : bank - stake;

			data.push({
				y: [bankBefore, bank, bankBefore, bank],
				x: new Date(date[0].replace(/st|nd|rd|th/g, '')),
				markerColor: !isLost ? '#6B8E23' : 'red'
			});
			return data;
		}, []);

		var chart = new CanvasJS.Chart(document.getElementById('chartContainer'), {
			animationEnabled: true,
			zoomEnabled: true,
			title: {
				text: 'Try Zooming and Panning'
			},
			axisX: {
				interval: 1,
				intervalType: 'day',
				valueFormatString: 'DDD'
			},
			axisY: {
				includeZero: false,
				title: 'Bank ($)',
				prefix: '$'
			},
			data: [
				{
					type: 'candlestick',
					yValueFormatString: '$##0.00',
					dataPoints: bankData
				}
			]
		});
		chart.render();
	});

