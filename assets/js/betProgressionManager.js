// Define default server port
const port = 5000;

// Scrapped user profiles
const user = {
	matemal1:     `http://localhost:${port}/public/json/matemal1.json`,         // Gold User
	mayerdud:     `http://localhost:${port}/public/json/mayerdud.json`,         // Silver user
	lbechtelar:   `http://localhost:${port}/public/json/virgilleffler.json`,    // Silver user
	castr0:       `http://localhost:${port}/public/json/castr0.json`            // Bronze user
	// ... Add here other profiles
};

// Which user profile to test
let url = user.matemal1;

const manager = {
	bank: 1000, // Total money available.
	stake: 100, // Current stake to be divided between each match from a set.
	rest: 0, // Debt to be recovered from next sets.
	topRest: 0, // The most money we had to take from bank to bet the next set.
	GAME_STATUS: {
		WIN: 1,
		LOST: 0,
		DRAW: 2
	},


	/**
	 *
	 * @param {Number} idx
	 * @param {Array} array
	 */
	process(idx, array) {
		let [date, odds, results] = array;

		// How many matches we have in one set.
		let matches = odds.length;

		// We have to recover the rest.
		// Add the rest to current stake.
		// Total stake we have to use in the next set.
		let totalStake = this.stake + this.rest;

		// Divide the rest between each match in this set
		let matchRest = this.rest / matches;

		// Divide the stake between each match in this set and add the rest.
		let matchStake = this.stake / matches + matchRest;

		// Check if we have enough money in bank to process this set.
		if (this.bank - totalStake > 0) {
			// If we do, take the money to run another set.
			this.bank -= totalStake;
			console.info(
				`%c[${idx}]. ${date[0]} | BANK (${totalStake.toFixed(2)}) | Stake ${
					this.stake.toFixed(2)} | Rest ${this.rest.toFixed(2)}`,
				`font-weight: bold; background-color: #B5CAF3;`
			);
		} else {
			console.error(`
				Not enough money. Bank has ${this.bank.toFixed(2)}. Minimum needed ${matchStake.toFixed(2)}`
			);
			throw new Error('Something went badly wrong!');
		}

		// Run each set to process it.

		for (let i = 0; i < matches; i++) {
			// Current match
			const resultStatus = results[i];

			// Check if it won
			if (resultStatus === this.GAME_STATUS.WIN) {
				// The return of money for this game
				let income = (odds[i] / 100) * matchStake;

				// Remove this match's debt from rest so we won't carry it over to other sets.
				this.rest -= matchRest;

				// Remove the negative 'minus' symbol if outcome is 0
				let minus = matchRest === 0 ? '' : '-';

				console.info(
					`    %cWIN.%c  Rest ${minus}${matchRest.toFixed(2)} | Total Rest: ${
						this.rest.toFixed(2)} | Balance: ${this.bank.toFixed(2)} +${income.toFixed(2)} `,
					`color: green; font-weight: bold; background-color: #D3ECE4;`,
					'color: black; background-color: #D3ECE4;'
				);

				// Deposit this outcome back to bank.
				this.bank += income;
			}

			// Check if it lost
			if (resultStatus === this.GAME_STATUS.LOST) {
				// Because it lost we have to retreieve the sum from next sets.
				// Store what we've lost to be recovered.
				this.rest += matchStake;
				console.info(
					`    %cLOST.%c Rest +${matchStake.toFixed(2)} | Total Rest: ${this.rest.toFixed(2)}`,
					`color: red; font-weight: bold; background-color: #F3B5B5;`,
					'color: black; background-color: #F3B5B5;'
				);
			}

			// Check if it's a draw
			if (resultStatus === this.GAME_STATUS.DRAW) {
				// Return this match stake back to bank.
				// This game is considered a draw and money will be returned to bank.
				console.info(
					`    %cDRAW.%c           Total Rest: ${this.rest.toFixed(2)} | Balance: ${
						this.bank.toFixed(2)} +${matchStake.toFixed(2)}. Money returned back to bank. `,
					`color: orange; font-weight: bold; background-color: #F6EACD;`,
					'color: black; background-color: #F6EACD;'
				);

				// Return this outcome back to bank.
				this.bank += matchStake;
			}
		}

		if (this.topRest < this.rest) {
			this.topRest = this.rest;
			console.warn(`[NEW TOP REST]: ${this.topRest} | Balance: ${this.bank.toFixed(2)}`);
		}
		console.info(
			`    --------------- Total REST: ${this.rest.toFixed(2)} |%c Total Balance: ${
				this.bank.toFixed(2)}`,
			`background-color: #E7E7E7;`
		);
	}
};

fetch(url)
	.then(function (response) {
		return response.json();

	})
	.then(function (data) {
		let bets = data;

		for (let i = 0; i < bets.length; i++) {
			manager.process(i + 1, bets[i], 1);
		}
		console.log(`The most rest was: ${manager.topRest}`);
	});

