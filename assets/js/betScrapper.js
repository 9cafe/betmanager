// Bet Scrapper for
// https://www.tipstersportal.com/{tipster}

let list = [];

document.querySelectorAll('.tipsBox').forEach((element, idx) => {
	let tipsBox = element; // 1,2,3
	let currentDate = tipsBox.children[0].innegrText;
	let ul = tipsBox.children[1];

	let date = [currentDate];
	let matches = [];
	let results = [];
	ul.querySelectorAll('li').forEach(li => {
		let odd = li.children[1].children[1].innerText * 100;
		let status = li.children[0].children[0].children[0].attributes[3].textContent;

		if (status === 'Win') {
			results.push(1);
		}

		if (status === 'Loss') {
			results.push(0);
		}

		if (status === 'Draw') {
			results.push(2);
		}

		matches.push(odd);
	});
	list.push([date, matches, results]);
});
