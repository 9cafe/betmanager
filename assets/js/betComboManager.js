// Define default server port
const port = 5000;

// Scrapped user profiles
const user = {
	matemal1:     `http://localhost:${port}/public/json/matemal1.json`,         // Gold User
	mayerdud:     `http://localhost:${port}/public/json/mayerdud.json`,         // Silver user
	lbechtelar:   `http://localhost:${port}/public/json/virgilleffler.json`,    // Silver user
	castr0:       `http://localhost:${port}/public/json/castr0.json`            // Bronze user
	// ... Add here other profiles
};

// Which user profile to test
let url = user.lbechtelar;

// How many matches have to be in a set to consider betting.
let minSet = 1;

const STATUSES = {
	1: 'WIN',
	0: 'LOST',
	2: 'DRAW',
};

class Manager {
	static GAME_STATUS = {
		WIN: 1,
		LOST: 0,
		DRAW: 2
	};

	constructor(bets = [], bank = 1000, stake = 100) {
		this.bets = [].concat(bets);
		this.bank = bank;       // Total money available.
		this.stake = stake;     // Current stake to be divided between each match from a set.
		this.rest = 0;          // Debt to be recovered from next sets.
		this.topRest = 0;       // The most money we had to take from bank to bet the next set.
		this.allGameStats = {
			losingStrike: {
				startDate: null,
				endDate: null,
				running: 0,
				max: 0
			},
			winningStrike: {
				startDate: null,
				endDate: null,
				running: 0,
				max: 0
			},
			bestWin: 0
		};
	}

	/**
	 *
	 *
	 * @param {Number} idx
	 * @param {Array<Array>} matchData
	 * @param {Number} min
	 * @returns {Array<String|Number>} processed matches
	 * @memberof Manager
	 */
	processCombo(idx, matchData, min) {
		const [date, odds, results] = matchData;

		// How many matches have to be in a set to consider betting.
		if (odds.length >= min) {
			if (this.moneyInBank(idx, date[0])) return this.processSet(idx, matchData);
		} else {
			alert(`${odds} | Not enough matches in this. Skipping.`);
			return [];
		}

		if (odds.length !== results.length) {
			// THIS SHOULD NEVER HAPPEN
			alert(`${e} check this set [${odds}] with [${results}] `);
			return [];
		}
	}

	/**
	 *
	 *
	 * @param {Number} betIdx
	 * @param {String} date
	 * @returns boolean
	 * @memberof Manager
	 */
	moneyInBank(betIdx, date) {
		if (this.bank - this.stake > 0) {
			console.info(
				`%c[${betIdx + 1}]. ${date} BANK (${this.stake.toFixed(2)}) | Stake ${this.stake.toFixed(
					2
				)} | Rest ${this.rest.toFixed(2)}`,
				`font-weight: bold; background-color: #B5CAF3;`
			);
		} else {
			console.error(
				`Not enough money. Bank has ${this.bank.toFixed(2)}. Minimum needed ${this.stake.toFixed(
					2
				)}`
			);
			alert(`Not enough money. Bank has ${this.bank.toFixed(2)}. Minimum needed ${this.stake.toFixed(
				2
			)}`);
		}
		return this.bank - this.stake > 0;
	}

	/**
	 *
	 *
	 * @param {Number} betIdx
	 * @param {Array} [[date] = d, odds, results]
	 * @returns
	 * @memberof Manager
	 */
	processSet(betIdx, [[date] = d, odds, results]) {
		let oddCombo = 1;
		const { winningStrike, losingStrike } = this.allGameStats;
		const setIsLost = results.some(v => v === Manager.GAME_STATUS.LOST);
		const matchIsDraw = results.every(v => v === Manager.GAME_STATUS.DRAW);
		const ticketResult = [betIdx + 1, date];

		if (matchIsDraw) {
			ticketResult.push(STATUSES[Manager.GAME_STATUS.DRAW]);
		} else if (setIsLost) {
			winningStrike.running = 0; // reset
			losingStrike.running += 1; // increment
			ticketResult.push(STATUSES[Manager.GAME_STATUS.LOST]);
			if (losingStrike.running >= losingStrike.max) {
				losingStrike.endDate = this.bets[betIdx - losingStrike.max][0][0];
				losingStrike.startDate = date;
				losingStrike.max = losingStrike.running;
			}
		} else {
			losingStrike.running = 0;   // reset
			winningStrike.running += 1; // increment
			ticketResult.push(STATUSES[Manager.GAME_STATUS.WIN]);
			if (winningStrike.running >= winningStrike.max) {
				winningStrike.endDate = this.bets[betIdx - winningStrike.max][0][0];
				winningStrike.startDate = date;
				winningStrike.max = winningStrike.running;
			}
		}

		for (let i = 0; i < odds.length; i++) {
			// Current match
			const resultStatus = results[i];
			const matchOdd = odds[i] / 100;

			// Check if it won
			if (resultStatus === Manager.GAME_STATUS.WIN) {
				// The return of money for this game

				oddCombo *= matchOdd;
				console.info(
					`    %cWIN.%c  Odds ${odds[i]}`,
					`color: green; font-weight: bold; background-color: #D3ECE4;`,
					'color: black; background-color: #D3ECE4;'
				);
			}
			// Check if it lost
			if (resultStatus === Manager.GAME_STATUS.LOST) {
				oddCombo *= matchOdd;
				console.info(
					`    %cLOST.%c Odds ${odds[i]}`,
					`color: red; font-weight: bold; background-color: #F3B5B5;`,
					'color: black; background-color: #F3B5B5;'
				);

				this.rest += this.stake;
				console.info(
					`    %cCOMBO LOST.%c | Combined Odd: ${oddCombo.toFixed(
						2
					)} | Balance: ${this.bank.toFixed(2)} Current rest is ${this.rest}`,
					`color: red; font-weight: bold; background-color: #F3B5B5;`,
					'color: black; background-color: #F3B5B5;'
				);
				if (this.topRest < this.rest) {
					this.topRest = this.rest;
					console.warn(
						`[${betIdx}] [NEW TOP REST]: ${this.topRest} | Balance: ${this.bank.toFixed(2)}`
					);
				}
				continue;
			}
			// Check if it's a draw
			if (resultStatus === Manager.GAME_STATUS.DRAW) {
				oddCombo = 1;
				console.info(
					`    %cDRAW.%c Odds ${odds[i]} | Skipping.`,
					`color: orange; font-weight: bold; background-color: #F6EACD;`,
					'color: black; background-color: #F6EACD;'
				);
			}
		}
		ticketResult.push(oddCombo);
		const income = setIsLost ? -this.stake : this.stake * oddCombo - this.stake;
		ticketResult.push(income.toFixed(2));
		ticketResult.push(this.rest.toFixed(2));
		if (income > this.allGameStats.bestWin) {
			this.allGameStats.bestWin = income;
		}
		// Update balance with amount won or lost
		this.bank += income;
		ticketResult.push(this.bank.toFixed(2));

		console.info(
			`    %cCOMBO WON.%c | Combined Odd: ${oddCombo.toFixed(2)} | Balance: ${this.bank.toFixed(
				2
			)} | Current rest was ${this.rest}`,
			`color: green; font-weight: bold; background-color: #D3ECE4;`,
			'color: black; background-color: #D3ECE4;'
		);
		this.rest = 0;
		return ticketResult;
	}

}

fetch(url)
	.then(r => r.json())
	.then(bets => {
		const manager = new Manager(bets.reverse());
		const table = document.getElementById('hook-matches');
		for (let i = 0; i < bets.length; i++) {
			let tr = document.createElement('tr');
			const matchResult = manager.processCombo(i, bets[i], minSet);
			for (let j = 0; j < matchResult.length; j++) {
				let td = document.createElement('td');
				td.innerText = matchResult[j];
				tr.appendChild(td);
			}
			table.appendChild(tr);
		}
		console.table(manager.allGameStats);
		console.log(`The most rest was: ${manager.topRest}`);
	});