# Bet Manager 

Bet Manager is a console tool to write and test (soccer betting) strategies using scrapped history data from https://tipstersportal.com 

The end purpose of this tool is to test which users are more profitable using different bet strategies.

### 1. Setup

1. [Clone the project locally](https://bitbucket.org/9cafe/betmanager/src/master/)
2. From the root of the project, open up a terminal and either run `yarn install` (if you have
**yarn** installed) or `npm install` to add the required external project dependencies.
3. Run the app with a local server by running in the terminal

### 2. Running

From the root of the project, open up a terminal and run `npm start` to run the code with a local server (default port is 5000).

## Getting started

### 3. Scrapping data

You can already run a strategy using profiles we already scrapped in `./public/json/...`. If you want to do so, jump to step 4. 

Or you can scrap new data using `betScrapper.js` script.

1. To do so you have to enter https://tipstersportal.com/ and choose a user profile you want to scrap;
2. Once your URL looks like this `https://tipstersportal.com/{YourUser}`, you can open devtools console and copy/paste the code from `betScrapper.js` and run it;
3. It will return a `list <array>`;
4. Store the result in a global variable and copy the result in a new json file in `./public/json/`
5. Open the strategy file you want to test a user history data and store the path link to a new variable, something like this:
`const yourUser = http://localhost:${port}/public/json/yourUser.json;`
6. At the very top of you strategy file, make sure you defined the correct local server port. Default would be 5000.

### 4. Testing a strategy

Currently there are two strategies. 

Both strategies have a global `bank <number>` and a fixed `stake <number>` to be bet on every set.

- Progression Manager `betProgressionManager.js`

>On each day we have a set of matches we have to bet on. We bet on each individual match using a `stake` divided by the number of matches from the set.
>
>The result is returned to `bank`.
>
>Each individual stake we lose on failed bets is stored in a `rest <number>` variable to be recovered. On the next event we add the `rest` to the `stake` variable the next day and divide it again for each match.

- Combo Manager `betComboManager.js`

> On each day we have a set of matches we have to bet on. We bet the whole `stake` to the entire set of matches to have accumulative odds, not individual bets. 
> One failed match will result in the lost of the entire set and thus loosing the entire stake.

You can inspect each result in the console. Additionally `betComboManager.js` has a graph implemented.

